# `graphql-server-customer`

> A simple GraphQL demonstration using [Apollo Server](https://www.apollographql.com/docs/apollo-server/).

## Publications

This repository is related to the following DZone.com publications:

* https://dzone.com/articles/when-its-time-to-give-rest-a-rest

To read more of my publications or browse my public projects, please review one of the following URLs:

* https://dzone.com/authors/johnjvester
* https://gitlab.com/users/johnjvester/projects

## Using the `graphql-server-customer`

Simply clone the repository and run the following command:

```shell
npm install
```

To start the server, run the following command:

```shell
npm start
```
The server will start on port 4000. You can access the Apollo Studio with your browser at the following URL:

http://localhost:4000/

## Heroku Settings

See the following documentation for more information on running Apollo server on Heroku:

https://www.apollographql.com/docs/apollo-server/deployment/heroku/

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
