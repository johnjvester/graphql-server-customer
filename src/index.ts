import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';

const typeDefs = `#graphql
  type Customer {
    token: String
    name: String
    sic_code: String
  }
  
  type Address {
    token: String
    customer_token: String
    address_line1: String
    address_line2: String
    city: String
    state: String
    postal_code: String
  }
  
  type Contact {
    token: String
    customer_token: String
    first_name: String
    last_name: String
    email: String
    phone: String
  }
  
  type Credit {
    token: String
    customer_token: String
    credit_limit: Float
    balance: Float
    credit_score: Int
  }

  type Query {
    addresses: [Address]
    address(customer_token: String): Address
    contacts: [Contact]
    contact(customer_token: String): Contact
    customers: [Customer]
    customer(token: String): Customer
    credits: [Credit]
    credit(customer_token: String): Credit
  }
`;

const resolvers = {
    Query: {
        addresses: () => addresses,
        address: (parent, args, context) => {
            const customer_token = args.customer_token;
            return addresses.find(address => address.customer_token === customer_token);
        },
        contacts: () => contacts,
        contact: (parent, args, context) => {
            const customer_token = args.customer_token;
            return contacts.find(contact => contact.customer_token === customer_token);
        },
        customers: () => customers,
        customer: (parent, args, context) => {
            const token = args.token;
            return customers.find(customer => customer.token === token);
        },
        credits: () => credits,
        credit: (parent, args, context) => {
            const customer_token = args.customer_token;
            return credits.find(credit => credit.customer_token === customer_token);
        }
    },
};

const server = new ApolloServer({
    typeDefs,
    resolvers,
});

const { url } = await startStandaloneServer(server, {
    listen: { port: Number.parseInt(process.env.PORT) || 4000 },
});

console.log(`Apollo Server ready at: ${url}`);

const customers = [
    {
        token: 'customer-token-1',
        name: 'Acme Inc.',
        sic_code: '1234'
    },
    {
        token: 'customer-token-2',
        name: 'Widget Co.',
        sic_code: '5678'
    }
];

const addresses = [
    {
        token: 'address-token-1',
        customer_token: 'customer-token-1',
        address_line1: '123 Main St.',
        address_line2: '',
        city: 'Anytown',
        state: 'CA',
        postal_code: '12345'
    },
    {
        token: 'address-token-22',
        customer_token: 'customer-token-2',
        address_line1: '456 Elm St.',
        address_line2: '',
        city: 'Othertown',
        state: 'NY',
        postal_code: '67890'
    }
];

const contacts = [
    {
        token: 'contact-token-1',
        customer_token: 'customer-token-1',
        first_name: 'John',
        last_name: 'Doe',
        email: 'jdoe@example.com',
        phone: '123-456-7890'
    }
];

const credits = [
    {
        token: 'credit-token-1',
        customer_token: 'customer-token-1',
        credit_limit: 10000.00,
        balance: 2500.00,
        credit_score: 750
    }
];